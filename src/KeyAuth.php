<?php

namespace Drupal\cmrf_key_authentication;

use Drupal\cmrf_core\Call;
use Drupal\cmrf_core\Core;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class KeyAuth.
 *
 * Handles all functionality regarding key authentication.
 */
class KeyAuth implements KeyAuthInterface {
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var array
   */
  protected $userObjects = [];

  /**
   * @var \Drupal\cmrf_core\Core
   */
  protected $core;

  /**
   * Constructs a new KeyAuth object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\SessionConfigurationInterface $session_configuration
   *   The session configuration.
   * @param \Drupal\cmrf_core\Core $core
   *   The CMRF Core class
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, Core $core, TranslationInterface $translation, MessengerInterface $messenger) {
    $this->config = $config_factory->get('cmrf_key_authentication.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->core = $core;
    $this->setStringTranslation($translation);
    $this->setMessenger($messenger);
  }

  /**
   * Checks whether the key and the email are in the query part of the url.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   */
  public function hasKeyInUrlQuery(Request $request) {
    // Extract the paramater name.
    $url_key_param_name = $this->config->get('url_key_param_name');
    $url_email_param_name = $this->config->get('url_email_param_name');
    // Check if query detection is enabled.
    if ($request->query->has($url_key_param_name) && $request->query->has($url_email_param_name)) {
      return true;
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function getKey(Request $request) {
    // Extract the paramater name.
    $url_key_param_name = $this->config->get('url_key_param_name');
    $url_email_param_name = $this->config->get('url_email_param_name');
    $logout_after_seconds = $this->config->get('logout_after') * 60;

    // Check if query detection is enabled.
    if (($key = $request->query->get($url_key_param_name)) && ($email = $request->query->get($url_email_param_name))) {
      $this->storeKeyIntoSession($key, $request);
      $this->storeEmailIntoSession($email, $request);
      $this->storeLoginTime($request);
      return $key;
    }

    if ($request->hasSession() && $key = $request->getSession()->get('cmrf_key_authentication_key')) {
      $loginTime = $request->getSession()->get('cmrf_key_authentication_login_time', 0);
      $currentTime = \Drupal::time()->getRequestTime();
      if (($loginTime + $logout_after_seconds) > $currentTime) {
        $this->storeLoginTime($request);
        return $key;
      } else {
        $this->clearSession();
      }
    }

    return FALSE;
  }

  /**
   * Login with a key
   *
   * @param $key
   * @param Request $request
   *
   * @return bool
   */
  public function loginWithKey($key, Request $request, $showInactivityLogoutMessage = true) {
    $this->storeKeyIntoSession($key, $request);
    if ($this->getUserByKey($key, $showInactivityLogoutMessage)) {
      $this->storeLoginTime($request);
      return true;
    }
    return false;
  }

  /**
   * Send an e-mail with the login code.
   *
   * @param $email
   * @param Request $request
   *
   * @return bool
   */
  public function sendKey($email, Request $request) {
    $connector = $this->config->get('civicrm_connector');
    $apiEntity = $this->config->get('civicrm_login_code_api_entity');
    $apiAction = $this->config->get('civicrm_login_code_api_action');
    $emailField = $this->config->get('civicrm_login_code_email_field');
    $ipField = $this->config->get('civicrm_login_code_ip_field');
    // Get fields API call.
    $params = [
      $emailField => $email
    ];
    if (strlen($ipField) && strlen(\Drupal::request()->getClientIp())) {
      $params['ip'] = \Drupal::request()->getClientIp();
    }
    $call = $this->core->createCall(
      $connector,
      $apiEntity,
      $apiAction,
      $params,
      []
    );

    // Execute call.
    $this->core->executeCall($call);
    if ($call->getStatus() == Call::STATUS_DONE) {
      $reply = $call->getReply();
      if (empty($reply['is_error'])) {
        $this->storeEmailIntoSession($email, $request);
        return TRUE;
      }
    }
    return false;
  }

  /**
   * Returns the e-mail address from the session
   * or false when email does not exist
   *
   * @return string|false
   */
  public function getEmailFromSession() {
    $email = '';
    $request = \Drupal::request();
    if ($request->hasSession() && $request->getSession()->has('cmrf_key_authentication_email')) {
      $email = $request->getSession()->get('cmrf_key_authentication_email');
    }
    if (!empty($email)) {
      return $email;
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserByKey($key, $showInactivityLogoutMessage = true) {
    $email = $this->getEmailFromSession();
    if (empty($email)) {
      return null;
    }
    if (isset($this->userObjects[$email][$key])) {
      return $this->userObjects[$email][$key];
    }

    $mapping = json_decode($this->config->get('mapping'), true);
    if (empty($mapping)) {
      $mapping = [];
    }
    $connector = $this->config->get('civicrm_connector');
    $apiEntity = $this->config->get('civicrm_api_entity');
    $apiAction = $this->config->get('civicrm_api_action');
    $keyField = $this->config->get('civicrm_key_field');
    $emailField = $this->config->get('civicrm_email_field');
    $ipField = $this->config->get('civicrm_ip_field');
    $params = [
      $keyField => $key,
      $emailField => $email,
    ];
    if (strlen($ipField) && strlen(\Drupal::request()->getClientIp())) {
      $params['ip'] = \Drupal::request()->getClientIp();
    }
    // Get fields API call.
    $call = $this->core->createCall(
      $connector,
      $apiEntity,
      $apiAction,
      $params,
      ['limit' => 0]
    );

    // Execute call.
    $this->core->executeCall($call);
    if ($call->getStatus() == Call::STATUS_DONE) {
      $reply = $call->getReply();
      if (isset($reply['count']) && $reply['count'] == 1) {
        $userData = $reply;
        if (isset($reply['values']) && is_array($reply['values']) && count($reply['values']) == 1) {
          $userData = reset($reply['values']);
        }
        if (isset($userData['code']) && !empty($userData['code'])) {
          $this->storeKeyIntoSession($userData['code'], \Drupal::request());
        }
        $this->userObjects[$email][$key] = Account::createFromCiviCRMData($userData, $email, $mapping);
        return $this->userObjects[$email][$key];
      }
    }
    $this->clearSession($showInactivityLogoutMessage);
    return null;
  }

  /**
   * Return the API fields for the authentication api
   *
   * @return array
   */
  public function getApiFields() {
    $connector = $this->config->get('civicrm_connector');
    $apiEntity = $this->config->get('civicrm_api_entity');
    $apiAction = $this->config->get('civicrm_api_action');
    $getFieldsApiAction = $this->config->get('civicrm_get_fields_action');
    // Get fields API call.
    $call = $this->core->createCall(
      $connector,
      $apiEntity,
      $getFieldsApiAction,
      ['api_action' => $apiAction],
      ['cache' => '1 hour']
    );

    // Execute call.
    try {
      $this->core->executeCall($call);
      if ($call->getStatus() == Call::STATUS_DONE) {
        $reply = $call->getReply();
        if (isset($reply['values'])) {
          return $reply['values'];
        }
      }
    } catch (\Exception $e) {
      // Do nothing.
    }

    return [];
  }

  /**
   * Clear the session.
   *
   * @return void
   */
  public function clearSession($showLogoutMessage = true) {
    $request = \Drupal::request();
    if ($request->hasSession()) {
      $request->getSession()->remove('cmrf_key_authentication_key');
      $request->getSession()->remove('cmrf_key_authentication_email');
      $request->getSession()->remove('cmrf_key_authentication_login_time');
    }
    if ($showLogoutMessage) {
      $this->messenger->addStatus($this->t('You have been logged out due to inactivity.'));
    }
  }

  private function storeKeyIntoSession($key, Request $request) {
    if ($request->hasSession()) {
      $request->getSession()->set('cmrf_key_authentication_key', $key);
    }
  }

  private function storeLoginTime(Request $request) {
    if ($request->hasSession()) {
      $request->getSession()->set('cmrf_key_authentication_login_time', \Drupal::time()->getRequestTime());
    }
  }

  private function storeEmailIntoSession($email, Request $request) {
    if ($request->hasSession()) {
      $request->getSession()->set('cmrf_key_authentication_email', $email);
    }
  }

}
