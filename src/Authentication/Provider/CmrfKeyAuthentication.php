<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_key_authentication\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\cmrf_key_authentication\KeyAuthInterface;
use Symfony\Component\HttpFoundation\Request;

class CmrfKeyAuthentication implements AuthenticationProviderInterface {

  /**
   * The key auth service.
   *
   * @var \Drupal\cmrf_key_authentication\KeyAuthInterface
   */
  protected $keyAuth;

  /**
   * Constructs a key authentication provider object.
   *
   * @param \Drupal\cmrf_key_authentication\KeyAuthInterface $key_auth
   *   The key auth service.
   */
  public function __construct(KeyAuthInterface $key_auth) {
    $this->keyAuth = $key_auth;
  }

  /**
   * Checks whether suitable authentication credentials are on the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE if authentication credentials suitable for this provider are on the
   *   request, FALSE otherwise.
   */
  public function applies(Request $request) {
    return (bool) $this->keyAuth->getKey($request);
  }

  /**
   * Authenticates the user.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request object.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   AccountInterface - in case of a successful authentication.
   *   NULL - in case where authentication failed.
   */
  public function authenticate(Request $request) {
    $user = \Drupal::currentUser()->getAccount();
    if ($this->keyAuth->hasKeyInUrlQuery($request) && $user->isAuthenticated()) {
      user_logout();
    }
    // Get the provided key.
    if ($key = $this->keyAuth->getKey($request)) {
      // Find the linked user.
      if ($user = $this->keyAuth->getUserByKey($key)) {
        // Return the user.
        return $user;
      }
    }
    return NULL;
  }

}
