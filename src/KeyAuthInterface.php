<?php

namespace Drupal\cmrf_key_authentication;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Session\AccountInterface;

/**
 * Interface KeyAuthInterface.
 */
interface KeyAuthInterface {

  /**
   * Checks whether the key and the email are in the query part of the url.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   */
  public function hasKeyInUrlQuery(Request $request);

  /**
   * Get the key provided in the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return string|false
   *   The API key provided in the request, or FALSE if there was not one.
   */
  public function getKey(Request $request);

  /**
   * Load the user associated with a given key.
   *
   * @param string $key
   *   The API key to match to a user.
   * @param bool $showInactivityLogoutMessage
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The matching user entity, or NULL if there was no match.
   */
  public function getUserByKey($key, $showInactivityLogoutMessage);

  /**
   * Login with a key
   *
   * @param $key
   * @param Request $request
   * @param bool $showInactivityLogoutMessage
   *
   * @return bool
   */
  public function loginWithKey($key, Request $request, $showInactivityLogoutMessage = true);

  /**
   * Send an e-mail with the login code.
   *
   * @param $email
   * @param Request $request
   *
   * @return mixed
   */
  public function sendKey($email, Request $request);

  /**
   * Return the API fields for the authentication api
   *
   * @return array
   */
  public function getApiFields();

  /**
   * Clear the session.
   *
   * @return void
   */
  public function clearSession();

  /**
   * Returns the e-mail address from the session
   * or false when email does not exist
   *
   * @return string|false
   */
  public function getEmailFromSession();

}
