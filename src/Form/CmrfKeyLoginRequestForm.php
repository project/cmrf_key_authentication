<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_key_authentication\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CmrfKeyLoginRequestForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'cmrf_key_authentication.login_request_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $email = '';
    /** @var \Drupal\cmrf_key_authentication\KeyAuth $keyAuth */
    $keyAuth = \Drupal::service('cmrf_key_auth');
    if (!empty($keyAuth->getEmailFromSession())) {
      $email = $keyAuth->getEmailFromSession();
    }
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail address'),
      '#default_value' => $email,
      '#required' => TRUE,
      '#description' => $this->t('You will receive the login code on this e-mail address'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send login code'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\cmrf_key_authentication\KeyAuth $keyAuth */
    $keyAuth = \Drupal::service('cmrf_key_auth');
    if (!$keyAuth->sendKey($form_state->getValue('email'), $this->getRequest())) {
      $this->messenger()->addError($this->t('Failed to send the e-mail containing the login code. Did you enter an incorrect e-mail address?'));
    } else {
      $this->messenger()->addStatus($this->t('An e-mail containing the login code is send to your e-mail address.'));
      $form_state->setRedirect('cmrf_key_authentication.login_key_form');
    }
  }

}
