<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_key_authentication\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CmrfKeyLoginForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'cmrf_key_authentication.login_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $resendUrl = Url::fromRoute('cmrf_key_authentication.login_request')->toString();
    /** @var \Drupal\cmrf_key_authentication\KeyAuth $keyAuth */
    $keyAuth = \Drupal::service('cmrf_key_auth');
    if (empty($keyAuth->getEmailFromSession())) {
      $response = new RedirectResponse($resendUrl);
      $response->send();
    }
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Code'),
      '#default_value' => '',
      '#required' => TRUE,
      '#description' => $this->t('Code you have received in your e-mail'),
    ];
    $form['resend'] = [
      '#markup' => '<a href="' . $resendUrl . '">' . $this->t('Resend e-mail with the code or send it to a different e-mail address') . '</a>',
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\cmrf_key_authentication\KeyAuth $keyAuth */
    $keyAuth = \Drupal::service('cmrf_key_auth');
    if (!$keyAuth->loginWithKey($form_state->getValue('key'), $this->getRequest(), false)) {
      $this->messenger()->addError($this->t('Could not login'));
    } else {
      if (!$this->getRequest()->request->has('destination')) {
        $form_state->setRedirect('<front>');
      }
      else {
        $this->getRequest()->query->set('destination', $this->getRequest()->request->get('destination'));
      }
    }
  }

}
