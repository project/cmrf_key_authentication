<?php

namespace Drupal\cmrf_key_authentication\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class KeyAuthSettingsForm.
 */
class CmrfKeyAuthenticationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cmrf_key_authentication.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cmrf_key_authentication_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\cmrf_core\Core $core */
    $core = \Drupal::service('cmrf_core.core');
    $config = $this->config('cmrf_key_authentication.settings');
    $form['url_key_param_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Key Parameter name'),
      '#default_value' => $config->get('url_key_param_name'),
      '#required' => TRUE,
      '#description' => $this->t('The name of the parameter used to detect the key from the url (the part after ?).'),
    ];
    $form['url_email_param_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Email Parameter name'),
      '#default_value' => $config->get('url_email_param_name'),
      '#required' => TRUE,
      '#description' => $this->t('The name of the parameter used to detect the email from the url (the part after ?).'),
    ];
    $form['logout_after'] = [
      '#type' => 'number',
      '#title' => $this->t('Logout after (minutes)'),
      '#default_value' => $config->get('logout_after'),
      '#required' => TRUE,
      '#description' => $this->t('Automatically logout the user after x minutes of inactivity'),
    ];
    $form['civicrm_connector'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('- None -'),
      '#options' => $core->getConnectors(),
      '#title' => $this->t('CiviCRM Connector'),
      '#default_value' => $config->get('civicrm_connector'),
      '#required' => TRUE,
    ];
    $form['validating_key'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration of the API for validating the authentication key'),
    ];
    $form['validating_key']['civicrm_api_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Entity for authentication'),
      '#default_value' => $config->get('civicrm_api_entity'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM API Entity used for authentication of the key.'),
    ];
    $form['validating_key']['civicrm_api_action'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Action for authentication'),
      '#default_value' => $config->get('civicrm_api_action'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM API Action used for authentication of the key.'),
    ];
    $form['validating_key']['civicrm_email_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Email Field for authentication'),
      '#default_value' => $config->get('civicrm_email_field'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM field which holds the e-mail address for authentication.'),
    ];
    $form['validating_key']['civicrm_ip_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API IP Address Field for authentication'),
      '#default_value' => $config->get('civicrm_ip_field'),
      '#required' => FALSE,
      '#description' => $this->t('The CiviCRM field which holds the ip address for authentication.'),
    ];
    $form['validating_key']['civicrm_key_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Key Field for authentication'),
      '#default_value' => $config->get('civicrm_key_field'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM field which holds the key for authentication.'),
    ];
    $form['validating_key']['civicrm_get_fields_action'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM getFields API action'),
      '#default_value' => $config->get('civicrm_get_fields_action'),
      '#required' => TRUE,
      '#description' => $this->t('The GetFields action of the CiviCRM API Entity for authentication. Used for retrieving the user fields.'),
    ];
    $form['sending_key'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration of the API for sending the authentication key'),
    ];
    $form['sending_key']['civicrm_login_code_api_entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Entity'),
      '#default_value' => $config->get('civicrm_login_code_api_entity'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM API Entity used for sending the authentication key.'),
    ];
    $form['sending_key']['civicrm_login_code_api_action'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM API Action'),
      '#default_value' => $config->get('civicrm_login_code_api_action'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM API Action used for sending the authentication key.'),
    ];
    $form['sending_key']['civicrm_login_code_email_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM Email field'),
      '#default_value' => $config->get('civicrm_login_code_email_field'),
      '#required' => TRUE,
      '#description' => $this->t('The CiviCRM field which holds the email for sending the authentication key.'),
    ];
    $form['sending_key']['civicrm_login_code_ip_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CiviCRM IP field'),
      '#default_value' => $config->get('civicrm_login_code_ip_field'),
      '#required' => FALSE,
      '#description' => $this->t('The CiviCRM field which holds the IP Address for sending the authentication key.'),
    ];
    $form['mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map CiviCRM data to Drupal User data'),
    ];
    $form['mapping']['mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Map Drupal User field with a CiviCRM Field'),
      '#default_value' => $config->get('mapping'),
      '#required' => TRUE,
      '#description' => $this->t('Mapping of the CiviCRM api fields to Drupal user fields. Specified as a JSON string. E.g. {"mail":"email"}. Where mail is the drupal user field and email is the CiviCRM email field..'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cmrf_key_authentication.settings')
      ->set('url_key_param_name', $form_state->getValue('url_key_param_name'))
      ->set('url_email_param_name', $form_state->getValue('url_email_param_name'))
      ->set('logout_after', $form_state->getValue('logout_after'))
      ->set('civicrm_connector', $form_state->getValue('civicrm_connector'))
      ->set('civicrm_api_entity', $form_state->getValue('civicrm_api_entity'))
      ->set('civicrm_api_action', $form_state->getValue('civicrm_api_action'))
      ->set('civicrm_key_field', $form_state->getValue('civicrm_key_field'))
      ->set('civicrm_email_field', $form_state->getValue('civicrm_email_field'))
      ->set('civicrm_ip_field', $form_state->getValue('civicrm_ip_field'))
      ->set('civicrm_login_code_api_entity', $form_state->getValue('civicrm_login_code_api_entity'))
      ->set('civicrm_login_code_api_action', $form_state->getValue('civicrm_login_code_api_action'))
      ->set('civicrm_login_code_email_field', $form_state->getValue('civicrm_login_code_email_field'))
      ->set('civicrm_login_code_ip_field', $form_state->getValue('civicrm_login_code_ip_field'))
      ->set('civicrm_get_fields_action', $form_state->getValue('civicrm_get_fields_action'))
      ->set('mapping', $form_state->getValue('mapping'))
      ->save();
  }

}
