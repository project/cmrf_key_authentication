<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_key_authentication;

use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

class Account extends User implements AccountInterface {

  /**
   * @var array
   */
  protected $civicrmUserData = [];

  protected $mappedValues = [];

  /**
   * Constructs a new user session.
   *
   * @param array $civicrmUserData
   *   Array of the CiviCRM User Data
   * @param string $email
   * @param array $mapping
   *   Mapping of the Drupal user field => CiviCRM API Field
   */
  public static function createFromCiviCRMData(array $civicrmUserData, string $email, array $mapping) {
    $notMappableFields = ['id', 'uid', 'login', 'access', 'status'];
    $values = [
      'id' => 0,
      'uid' => 0,
      'status' => 1,
      'login' => \Drupal::time()->getRequestTime(),
      'access' => \Drupal::time()->getRequestTime(),
      'mail' => $email,
      'name' => $email,
      'roles' => [],
    ];
    foreach($mapping as $drupalField => $civicrmField) {
      if (!in_array($drupalField, $notMappableFields) && isset($civicrmUserData[$civicrmField])) {
        $values[$drupalField] = $civicrmUserData[$civicrmField];
      }
    }
    if (!isset($values['roles']) || !is_array($values['roles'])) {
      $values['roles'] = [AccountInterface::AUTHENTICATED_ROLE];
    } elseif (!in_array(AccountInterface::AUTHENTICATED_ROLE, $values['roles'])) {
      $values['roles'][] = AccountInterface::AUTHENTICATED_ROLE;
    }
    $user = new Account($values, 'user');
    $user->civicrmUserData = $civicrmUserData;
    $user->mappedValues = $values;
    return $user;
  }

  /**
   * Returns the value of the civicrm data.
   *
   * @param string $fieldName
   *
   * @return mixed|null
   */
  public function getCiviCRMValue(string $fieldName) {
    if (isset($this->civicrmUserData[$fieldName])) {
      return $this->civicrmUserData[$fieldName];
    }
    return null;
  }

  /**
   * Return the mapped values
   *
   * @return array
   */
  public function getMappedValues() {
    return $this->mappedValues;
  }

  /**
   * Returns the user ID or 0 for anonymous.
   *
   * @return int
   *   The user ID.
   */
  public function id() {
    return 0;
  }

  /**
   * Returns TRUE if the account is authenticated.
   *
   * @return bool
   *   TRUE if the account is authenticated.
   */
  public function isAuthenticated() {
    return true;
  }

  /**
   * Returns TRUE if the account is anonymous.
   *
   * @return bool
   *   TRUE if the account is anonymous.
   */
  public function isAnonymous() {
    return false;
  }

}
